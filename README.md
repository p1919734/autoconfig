# AUTOCONFIG


#### Pros.
* Easy to use
* Easy to setup
* Minimal


### INSTALLATION
Just download the git repository with `git clone https://forge.univ-lyon1.fr/p1919734/autoconfig.git`.


### UTILISATION
To install the provided configuration, execute `installer.bash` and Voilà! Files and directories are installed where they're supposed to.

To export your configuration into AUTOCONFIG (and transporting it easily on a usb-stick for exemple), execute `import-config.bash`.

AUTOCONFIG will ask you before overwriting existing files.


#### Features to come :
* Possibility to create symlink instead of copying directories/files
* Possibility to "tidy-up" with `stow`
* Adding yaml files to easely add directories or files to the configuration
* Cleaner code
