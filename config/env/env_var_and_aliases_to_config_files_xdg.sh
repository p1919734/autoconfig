#!/bin/sh

# Environment Variables Assignments for XDG Base Directory Specification
export XDG_CONFIG_HOME="$HOME"/.config
export XDG_CACHE_HOME="$HOME"/.cache
export XDG_DATA_HOME="$HOME"/.local/share

# Should not be set, read : https://wiki.archlinux.org/index.php/XDG_Base_Directory
#export XDG_RUNTIME_DIR=

# System Directories # Not defined if not needed
#export XDG_DATA_DIRS=/usr/local/share:/usr/share
#export XDG_CONFIG_DIRS=/etc/xdg

# Environment Variables Assignments for softwares
# This is needed to move config/cache/data files to be moved where they should be according to the XDG Base Directory Specification
# XDG_DATA_HOME
export ATOM_HOME="$XDG_DATA_HOME"/atom
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export HISTFILE="$XDG_DATA_HOME"/zsh/history
export HISTFILE="$XDG_DATA_HOME"/bash/history

export GRADLE_USER_HOME="$XDG_DATA_HOME"/gradle
# XDG_CONFIG_DIRS
export GTK_RC_FILES="$XDG_CONFIG_HOME"/gtk-1.0/gtkrc
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME"/npm/npmrc
export INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc
# XDG_CACHE_HOME

#export VIMINIT='source ~/.config/vim/vimrc'


# Disable LESS history file
export LESSHISTFILE=-

# Aliases for softwares
# This is needed to lauch software with the right config/cache/data files according to the XDG Base Directory Specification
alias yarn="yarn --use-yarnrc $XDG_CONFIG_HOME/yarn/config"
