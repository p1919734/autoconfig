" Plugins {{{

call plug#begin('~/.config/nvim/plugged')

Plug 'chrisbra/matchit'

" Utilitary
Plug 'tpope/vim-commentary' " Comment quickly lines
Plug 'godlygeek/tabular' " align things
Plug 'qpkorr/vim-renamer' " Bulk rename files
Plug 'w0rp/ale' " Syntax check
Plug 'mg979/vim-visual-multi', {'branch': 'master'} " multiple word select cursor
Plug 'preservim/nerdtree' " file tree
Plug 'xuyuanp/nerdtree-git-plugin' " addon of nerdtree with git status
Plug 'nathanaelkane/vim-indent-guides' " help visualize indentation
Plug 'tpope/vim-fugitive' " git integration



Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'wellle/targets.vim' " Ajoute des text objects. ~
Plug 'tpope/vim-surround' " Changing surround parenthesis. ~

Plug 'jpalardy/vim-slime' " REPL
Plug 'metakirby5/codi.vim' " Autre REPL (dans le buffer direct)

" Appearance
Plug 'junegunn/goyo.vim' " Purify interface
Plug 'luochen1990/rainbow' " Rainbow parentheses

" Languages
Plug 'vim-ruby/vim-ruby'
Plug 'docunext/closetag.vim' " Auto close tags in HTML
Plug 'PotatoesMaster/i3-vim-syntax'
Plug 'neovimhaskell/haskell-vim'

call plug#end() " }}}

" Plugin settings {{{
let g:rainbow_active = 1 "0 if you want to enable it later via :RainbowToggle

let g:slime_target = "tmux"
let g:slime_default_config = {"socket_name": "default", "target_pane": "{right-of}"}
let g:slime_dont_ask_default = 1

let g:fzf_tags_command = 'ctags -f .tags -R .'

let g:indent_guides_enable_on_vim_startup = 1


" }}}

" sets {{{
syntax on
set nocompatible
set nojoinspaces " when joining lines, don't insert two spaces after punctuation
set tags=./.tags;
set lazyredraw ttyfast " Améliore l'affichage
set backspace=indent,eol,start
set formatoptions+=j " when pressing J on comments, join only the text and not the comment delimiter


" XDG Base Specification
set undofile undodir=~/.local/share/nvim/undo " Maintain undo history between sessions
set directory=~/.local/share/nvim/swap
set backupdir=~/.local/share/nvim/backup
set viewdir=~/.local/share/nvim/view
set viminfo+='1000,n~/.local/share/nvim/viminfo



set splitright splitbelow diffopt=vertical
set number relativenumber
set incsearch hlsearch smartcase ignorecase

set wrap
set mouse=a
set showcmd showmatch " Montre les () <> {} [] correspondants
set wildmenu wildmode=full path+=** "Activer la recherche récursive
set clipboard+=unnamedplus
set foldmethod=marker foldmarker={{{,}}} foldlevel=0 " folds
set tabstop=4 shiftwidth=4 " tabs
set list listchars=tab:\|\ ,nbsp:~ showbreak=↳ " normally invisible chars

set enc=utf-8 fenc=utf-8 termencoding=utf-8 " UTF-8 everywhere !

if has("autocmd")
	filetype plugin indent on
endif
" }}}
" maps {{{
let mapleader = ','

" command {{{
" Allows to sudo a file to save it when sudo was forgot
cmap w!! %!sudo tee > /dev/null %
" }}}

" normal {{{
nnoremap j gj
nnoremap k gk
noremap n nzz
noremap N Nzz
nnoremap <silent> <leader><space> :nohlsearch<CR>

nmap <leader>mt :!ctags -f .tags -R .<CR>
nmap <leader>cs ma:%s/\s\+$//<CR>`a
nmap <leader><tab> :FZF<CR>
nmap 22 :call Launch()<CR>

" Spelling
nmap <leader>sf :setlocal spell! spelllang=fr<cr>
" z= → corrections, zg → ajout mot dico, zug → enlève mot dico
" ]s → prochain mauvais mot, [s → précédent

" Often edit these files
nmap <silent> <leader>ev :tabnew ~/.vimrc<CR>
nmap <silent> <leader>ez :tabnew ~/.zshrc<CR>
nmap <silent> <leader>sv :source ~/.vimrc<CR>:echo 'vimrc reloaded'<CR>

" create link in markdown
nmap <leader>ml ya[maGo<Esc>pa:<Esc>`a
" show syntax class of char under cursor
nmap <leader>ss :let s = synID(line('.'), col('.'), 1) \| echo synIDattr(s, 'name') . ' -> ' . synIDattr(synIDtrans(s), 'name')<CR>
" }}}
" insert {{{
imap jk <Esc>

" add more undo-steps when writing prose
inoremap . .<C-g>u<esc>:w<cr>a
inoremap ? ?<C-g>u
inoremap ! !<C-g>u
inoremap , ,<C-g>u

" pour les fichiers de password (oui je suis une feignasse)
inoremap ;pr autotype: user :tab pass :enter
" }}}
" visual {{{
xnoremap <silent> <Up> :m '<-2<CR>gv=gv
xnoremap <silent> <Down> :m '>+1<CR>gv=gv
" }}}
" }}}
" autocmds {{{
au Filetype haskell,markdown,scheme set expandtab
au FileType c,cpp setl ofu=ccomplete#CompleteCpp
au FileType css setl ofu=csscomplete#CompleteCSS
au BufEnter *.tex set ft=tex
autocmd Filetype cpp set foldmethod=syntax
" au Filetype markdown :Goyo
" }}}
" Launch() {{{
func! Launch()
	exec "w"
	if     &filetype == 'cpp'       | exec "!make"
	elseif &filetype == 'd'         | exec "!dmd -of=%:r.out % && ./%:r.out"
	elseif &filetype == 'java'      | exec "!javac % && java %:r"
	elseif &filetype == 'prolog'    | exec "!swipl %"
	elseif &filetype == 'php'       | exec "!php %"
	elseif &filetype == 'ruby'      | exec "!ruby %"
	elseif &filetype == 'python'    | exec "!python3 %"
	elseif &filetype == 'sh'        | exec "!bash %"
	elseif &filetype == 'rust'      | exec "!cargo run"
	elseif &filetype == 'html'      | exec "!firefox % &"
	elseif &filetype == 'c'         | exec "!make"
	elseif &filetype == 'tex'       | exec '!lualatex "%" && mupdf "%:r.pdf" &'
	elseif &filetype == 'xdefaults' | exec "!~/.dotfiles/bin/change_theme.sh dark"
	elseif &filetype == 'haskell'   | exec "!runhaskell %"
	elseif &filetype == 'vimwiki'   | exec "Vimwiki2HTMLBrowse"
	elseif &filetype == 'awk'       | exec "!awk -f % out.tr"

	elseif getcwd() =~ "/home/doshirae/permanent/web"
		exec "!(cd /home/doshirae/permanent/web && ./h)"
	endif
endfunc
" }}}

set background=dark
au Filetype php set expandtab

colorscheme koehler "_modified
