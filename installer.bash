#!/bin/bash

# Set the current directory to the script's current directory
SCRIPT_PATH=$(echo "$PWD"/"${0%/*}" | sed 's/\/\.//') # remove unnecessary '/./'
[ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ] && cd $SCRIPT_PATH


# Install Bash config files
bash_install() {
  cp -iv "$SCRIPT_PATH"/dotfiles/bashrc "$HOME"/.bashrc
}
# Install Zsh config files
zsh_install() {
  cp -iv "$SCRIPT_PATH"/dotfiles/zshrc "$HOME"/.zshrc
}
# Install Aliases config files
aliases_install() {
  cp -ivr "$SCRIPT_PATH"/config/aliases/ "$HOME"/.config
}
# Install env variables config
env_install() {
  cp -ivr "$SCRIPT_PATH"/config/env/ "$HOME"/.config
}
# Install Nvim config files and data directory and auto install vim-plug
nvim_install() {
  cp -iv "$SCRIPT_PATH"/config/nvim/init.vim "$HOME"/.config/nvim/init.vim
  # Create nvim data directories if they don't exist already
  [ ! -d "$HOME"/.local/share/nvim ] && mkdir -p "$HOME"/.local/share/nvim/{undo,swap,backup,view}
  [ ! -d "$HOME"/.local/share/nvim/undo ] && mkdir -p "$HOME"/.local/share/nvim/undo
  [ ! -d "$HOME"/.local/share/nvim/swap ] && mkdir -p "$HOME"/.local/share/nvim/swap
  [ ! -d "$HOME"/.local/share/nvim/backup ] && mkdir -p "$HOME"/.local/share/nvim/backup
  [ ! -d "$HOME"/.local/share/nvim/view ] && mkdir -p "$HOME"/.local/share/nvim/view
  # install vim-plug if its not already the case
  [ ! -d "$HOME"/.local/share/nvim/site/autoload/plug.vim ] && sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

}

# Install all config files
install_all(){
  bash_install
  zsh_install
  aliases_install
  env_install
  nvim_install
}

RCol='\e[0m' # Text Reset
BRed='\e[1;31m' # Bold red text

echo "Choose configuration to install (just press enter for all) :"
echo -e "[${BRed}1${RCol}] : bash \n[${BRed}2${RCol}] : zsh \n[${BRed}3${RCol}] : aliases \n[${BRed}4${RCol}] : environement variables \n[${BRed}5${RCol}] : NeoVim"

read selections
if [ -z "$selections" ]; then
  install_all
else
  for selection in $selections
  do
    case $selection in
      1 ) bash_install ;;
      2 ) zsh_install ;;
      3 ) aliases_install ;;
      4 ) env_install ;;
      5 ) nvim_install ;;
      0 ) exit ;;
      * ) echo "Wrong selection, exiting" ; exit ;;
    esac
  done
fi

echo "Finished!"
