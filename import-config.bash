#!/bin/bash

# Set the current directory to the script's current directory
SCRIPT_PATH=$(echo "$PWD"/"${0%/*}" | sed 's/\/\.//')
[ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ] && cd $SCRIPT_PATH


echo "Copying configuration files : "

# Copy Nvim config files
cp -iv "$HOME"/.config/nvim/init.vim "$SCRIPT_PATH"/config/nvim/init.vim
# Copy Bash config files
cp -iv "$HOME"/.bashrc "$SCRIPT_PATH"/dotfiles/bashrc
# Copy Zsh config files
cp -iv "$HOME"/.zshrc "$SCRIPT_PATH"/dotfiles/zshrc
# Copy env variables config
cp -ivr "$HOME"/.config/env "$SCRIPT_PATH"/config/
# Copy Aliases config files
cp -ivr "$HOME"/.config/aliases "$SCRIPT_PATH"/config/

echo "Finished!"
